import itertools
import os
from collections.abc import Generator
from enum import Enum

import numpy as np
import pandas as pd
import tiledbvcf

from common import dataset_read, pickle_cache
from genomes import CHROMOSOMES, NON_PAR_REGIONS, PAR_REGIONS, Region


class Sex(Enum):
    MALE = 0
    FEMALE = 1
    UNKNOWN = 2


# Heterezygosity rate for PAR regions
# Below 0.15 -> male
# Between 0.2 and 0.35 -> Unknown
# Above 0.35 -> Female
def determine_sex(het_rate):
    if het_rate < 0.15:
        return Sex.MALE
    elif het_rate > 0.35:
        return Sex.FEMALE
    else:
        return Sex.UNKNOWN


def compute_zygosity(fmt_gt: pd.Series) -> tuple[np.array, np.array]:
    """
    Compute zygosity from genotype array
    Only hetero- and homozygosity - hemizygotes is treated as homozygotes
    """
    arr = fmt_gt.array
    try:
        # Assume all arrays are the same size - this is a lot faster
        arr = np.stack(arr)
    except ValueError:
        # If not, we extend all 1-sized arrays with the same value (i.e. hemizygous)
        # Some callers call hemizygotes as [1], others as [1, 1] - this treats them as [1, 1]
        # Hemizygosity needs to be treated separately
        arr_single_gt = fmt_gt.apply(lambda x: x.size == 1)
        fmt_gt.loc[arr_single_gt] = fmt_gt.loc[arr_single_gt].apply(
            lambda x: np.append(x, x)
        )
        arr = np.stack(arr)

    het = np.sum(arr == 1, axis=1) == 1
    hom = np.sum(arr == 1, axis=1) == 2

    return het, hom


@pickle_cache(os.environ.get("SEX_BY_SAMPLE_ID_CACHE", None))
def sex_by_sample(ds: tiledbvcf.Dataset, cache: pd.Series | None) -> pd.Series:
    """Determine sex from heterezygosity rate in non-PAR regions of the X chromosome"""
    print("Computing sex by sample")
    samples = ds.samples()
    if cache is not None:
        samples = list(set(samples) - set(cache.index))
        sex = cache
    else:
        sex = pd.Series()
    for sample_batch in itertools.batched(samples, 50):
        par_res = pd.DataFrame()
        for par_res_part in ds.read_iter(
            attrs=["sample_name", "filters", "fmt_GT"],
            samples=sample_batch,
            regions=[str(x) for x in NON_PAR_REGIONS],
        ):
            # Include only PASS calls
            par_res_part = par_res_part[
                par_res_part["filters"].apply(lambda x: x[0]) == "PASS"
            ]

            # Count number of heterozygous calls - exclude non-calls
            het, _ = compute_zygosity(par_res_part["fmt_GT"])
            par_res_part["het"] = het
            par_res_part.drop(columns=["fmt_GT", "filters"], inplace=True)
            par_res = pd.concat([par_res, par_res_part])

        het_rate = par_res.groupby(["sample_name"]).mean()["het"]
        sex_part = het_rate.apply(determine_sex)
        sex = pd.concat([sex, sex_part])
        if not ds.read_completed():
            raise RuntimeError("Read not completed - need to iterate over all data")
    return sex


def iter_regions(region_size: int) -> Generator[Region, None, None]:
    """Iterate over all regions in the genome"""
    yield from sum((list(chrom.split(region_size)) for chrom in CHROMOSOMES), [])


@dataset_read
def variant_frequency(
    ds: tiledbvcf.Dataset, region_size: int
) -> Generator[pd.DataFrame, None, None]:
    """Calculate variant frequency for each variant in the dataset"""
    sex = sex_by_sample(ds)

    # Filter out samples with unknown sex
    sex.where(sex != Sex.UNKNOWN, inplace=True)
    sex.dropna(inplace=True)
    print(
        f"Computing variant frequency for {len(sex)} samples for regions of size {region_size}"
    )
    for region in iter_regions(region_size):
        res = pd.DataFrame()
        for res_part in ds.read_iter(
            attrs=[
                "contig",
                "pos_start",
                "alleles",
                "fmt_GT",
                "sample_name",
            ],
            regions=[str(region)],
            samples=sex.index.tolist(),
        ):
            if not len(res_part):
                continue

            res_part["ref"] = res_part["alleles"].str[0]
            res_part["alt"] = res_part["alleles"].str[1]

            # Set sex for each variant call, based on sample name
            res_part["sex"] = sex[res_part["sample_name"]].array

            # Filter out Female calls on the Y chromosome
            res_part = res_part[
                (~((res_part["contig"] == "chrY") & (res_part["sex"] == Sex.FEMALE)))
                | (~((res_part["contig"] == "Y") & (res_part["sex"] == Sex.FEMALE)))
            ]

            # Filter out non-calls
            het, hom = compute_zygosity(res_part["fmt_GT"])
            calls = het | hom
            hemi = False
            res_part = res_part[calls]
            het = het[calls]
            hom = hom[calls]
            res_part["autosomal"] = True

            # Treat PAR regions separately - determine autosomal status, and zygosity
            par_test = None
            for PAR_REGION in PAR_REGIONS:
                if par_test is None:
                    par_test = res_part["contig"] == PAR_REGION.contig
                else:
                    par_test = par_test | (res_part["contig"] == PAR_REGION.contig)
            if par_test is not None and par_test.any():
                res_part["autosomal"] = False
                for PAR_REGION in PAR_REGIONS:
                    res_part.loc[
                        (res_part["contig"] == PAR_REGION.contig)
                        & (res_part["pos_start"] >= PAR_REGION.start)
                        & (res_part["pos_start"] < PAR_REGION.end),
                        "autosomal",
                    ] = True

                # Determine zygosity
                hemi = (~res_part["autosomal"]) & (res_part["sex"] == Sex.MALE)
                het = ~hemi & het
                hom = ~hemi & hom

            # Validate zygosity calls
            multiple = (het & hom) | (het & hemi) | (hom & hemi)
            if multiple.any():
                raise RuntimeError(
                    "Multiple zygosity calls - this shouldn't be possible"
                )
            res_part["het"] = het
            res_part["hom"] = hom
            res_part["hemi"] = hemi

            no_zygosity = ~(res_part["het"] | res_part["hom"] | res_part["hemi"])
            if no_zygosity.any():
                raise RuntimeError("No zygosity call for some variants")

            res_part["AC"] = 1
            res_part.loc[res_part["hom"], "AC"] = 2

            # Drop no longer required columns
            res_part.drop(
                columns=["alleles", "fmt_GT", "sample_name", "sex"],
                inplace=True,
            )

            # Group by variant and sum AC and zygosity for this part
            res_part = res_part.groupby(
                ["contig", "pos_start", "ref", "alt", "autosomal"], as_index=False
            ).sum()

            # Concatenate with previous results
            res = pd.concat([res, res_part])

        if not len(res):
            continue

        # Group by variant and sum AC
        res = res.groupby(
            ["contig", "pos_start", "ref", "alt", "autosomal"], as_index=False
        ).sum()

        # Calculate AN - 2*number of samples for all autosomal regions
        # 2*number of females + number of males for all non-autosomal regions on X
        # Number of males on Y
        sex_counts = sex.value_counts()
        num_male = sex_counts[Sex.MALE] if Sex.MALE in sex_counts else 0
        num_female = sex_counts[Sex.FEMALE] if Sex.FEMALE in sex_counts else 0
        res["AN"] = 2
        res.loc[res["autosomal"], "AN"] = 2 * (num_female + num_male)
        res.loc[~res["autosomal"], "AN"] = 2 * num_female + num_male
        res.loc[((res["contig"] == "Y") | (res["contig"] == "chrY")), "AN"] = num_male
        res["AF"] = res["AC"] / res["AN"]

        res.drop(columns=["autosomal"], inplace=True)

        yield res
