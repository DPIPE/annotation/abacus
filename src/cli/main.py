import json
import os
import time

import bgzip
import click
import numpy as np
import tiledbvcf

from calculate_frequency import variant_frequency
from common import dataset_read
from ingest import ingest_samples


class JsonFromPath(click.Path):
    name = "jsonfile"

    def convert(self, value, param, ctx):
        value = super().convert(value, param, ctx)
        with open(value, "rt") as fh:
            return json.load(fh)


@click.group()
def cli():
    pass


@cli.command()
@click.option("--tile-capacity", default=20_000, type=int, help="Tile capacity")
@click.option("--anchor-gap", default=1000, type=int, help="Anchor gap")
@click.option(
    "--tiledb-config", type=JsonFromPath(exists=True), help="TileDB config as JSON file"
)
def create(tile_capacity: int, anchor_gap: int, tiledb_config: dict | None):
    """
    Create a new dataset with given configuration.

    See tiledb and tiledbvcf documentation for more information about configuration parameters.
    """
    ds = tiledbvcf.Dataset(
        os.environ["TILEDB_URI"], mode="w", tiledb_config=tiledb_config
    )
    ds.create_dataset(tile_capacity=tile_capacity, anchor_gap=anchor_gap)
    click.echo("Dataset created")


@cli.command()
@click.argument(
    "filenames",
    required=True,
    nargs=-1,
    type=click.Path(exists=True, resolve_path=True),
)
def deposit_samples(filenames):
    click.echo(f"Depositing {len(filenames)} samples")
    t1 = time.time()
    ingest_samples(filenames)
    t2 = time.time()
    click.echo(
        f"Deposited {len(filenames)} samples in {t2-t1:.2f} seconds (average {(t2-t1)/len(filenames):.2f} seconds per sample)"
    )


@cli.command()
@click.option("--output", "-o", help="Output VCF file", type=str, required=True)
@click.option("--suffix", "-s", default="", type=str, help="Suffix for INFO field")
@click.option(
    "--region-size",
    "-r",
    default=50_000_000,
    type=int,
    help="Batch size of regions to compute frequency for (lower values will decrease memory usage, but increase runtime)",
)
def extract_frequencies(output: str, suffix: str, region_size: int):
    """
    Extract allele frequencies from the dataset and write them to a VCF file.

    For each variant, the following INFO fields are computed:

    - AC: Allele count
    - AN: Allele number
    - AF: Allele frequency
    - Hom: Number of homozygotes
    - Het: Number of heterozygotes
    - Hemi: Number of hemizygotes

    If a suffix is provided, the INFO fields will be suffixed with the provided string.
    """

    VCF_TEMPLATE = (
        "##fileformat=VCFv4.2\n"
        """##INFO=<ID=AF{SUFFIX},Number=A,Type=Float,Description="Allele frequency (=AC{SUFFIX}/AN{SUFFIX})",Source="In-house database",Version="3.0">\n"""
        """##INFO=<ID=AC{SUFFIX},Number=A,Type=Integer,Description="Allele count. Number of observed alleles for given variant. (=2*Hom{SUFFIX}+Het{SUFFIX})",Source="In-house database",Version="3.0">\n"""
        """##INFO=<ID=Hom{SUFFIX},Number=A,Type=Integer,Description="Number of observed homozygotes for given variant",Source="In-house database",Version="3.0">\n"""
        """##INFO=<ID=Het{SUFFIX},Number=A,Type=Integer,Description="Number of observed heterozygotes for given variant",Source="In-house database",Version="3.0">\n"""
        """##INFO=<ID=Hemi{SUFFIX},Number=A,Type=Integer,Description="Number of observed hemizygotes for given variant",Source="In-house database",Version="3.0">\n"""
        """##INFO=<ID=AN{SUFFIX},Number=1,Type=Integer,Description="Allele number. Total number of alleles on position (=2*SAMPLE_COUNT in autosomal regions)",Source="In-house database",Version="3.0">\n"""
        """#CHROM POS ID REF ALT QUAL FILTER INFO\n"""
    )

    LINE_TEMPLATE = """{chrom}\t{pos}\t.\t{ref}\t{alt}\t5000\tPASS\t{info}"""
    INFO_TEMPLATE = "AC{SUFFIX}={ac};AN{SUFFIX}={an};AF{SUFFIX}={af:.4f};Hom{SUFFIX}={hom};Het{SUFFIX}={het};Hemi{SUFFIX}={hemi}"
    click.echo("Extracting frequencies")

    with open(output, "wb") as raw:
        with bgzip.BGZipWriter(raw) as fh:
            fh.write(VCF_TEMPLATE.format(SUFFIX=suffix).encode())
            N = 0
            for res in variant_frequency(region_size):
                for i, r in res.iterrows():
                    line = LINE_TEMPLATE.format(
                        chrom=r.contig,
                        pos=r.pos_start,
                        ref=r.ref,
                        alt=r.alt,
                        info=INFO_TEMPLATE.format(
                            SUFFIX=suffix,
                            ac=r.AC,
                            an=r.AN,
                            af=r.AF,
                            hom=r.hom,
                            het=r.het,
                            hemi=r.hemi,
                        ),
                    )
                    fh.write((line + "\n").encode())
                    N += 1
                    if N % 100000 == 0:
                        click.echo(
                            f"Written {N} variants (last position {r.contig}:{r.pos_start})"
                        )
                        fh.flush()
    click.echo(f"Computed frequencies for {N} variants")


@cli.command()
@click.argument("contig", type=str)
@click.argument("pos", type=int)
@click.argument("ref", type=str)
@click.argument("alt", type=str)
@dataset_read
def lookup(ds: tiledbvcf.Dataset, contig, pos, ref, alt):
    """
    Look up a variant by its position and ref/alt alleles.

    Will display samples having this variant, as well as some statistics about the variant.

    Note: Sex is not considered, and thus hemizygosity and allele number will be incorrect on sex chromosomes (excluding PAR regions).
    """
    res = ds.read(
        attrs=[
            "sample_name",
            "contig",
            "pos_start",
            "alleles",
            "filters",
            "fmt_GT",
            "fmt_AD",
            "fmt_GQ",
            "fmt_DP",
        ],
        regions=[f"{contig}:{pos}-{pos}"],
    )
    if not len(res):
        click.echo("Variant not found")
        exit()
    res[["ref", "alt"]] = res["alleles"].to_list()
    res = res[(res["ref"] == ref) & (res["alt"] == alt)]
    _, alt_count = zip(*res["fmt_AD"].to_list())
    res["AR"] = alt_count / res["fmt_DP"]
    res["AC"] = list(map(lambda x: np.count_nonzero(x == 1), res["fmt_GT"].to_list()))
    res.drop(columns=["alleles"], inplace=True)
    res = res[
        [
            "sample_name",
            "contig",
            "pos_start",
            "ref",
            "alt",
            "filters",
            "fmt_GT",
            "fmt_AD",
            "fmt_GQ",
            "fmt_DP",
            "AR",
            "AC",
        ]
    ]
    res.rename(
        columns={
            "pos_start": "pos",
            "fmt_GT": "GT",
            "fmt_AD": "AD",
            "fmt_GQ": "GQ",
            "fmt_DP": "DP",
        },
        inplace=True,
    )
    click.echo(res.to_string())

    het = res[res["AC"] == 1]
    hom = res[res["AC"] == 2]

    click.echo(f"Number of samples: {len(res)}")
    click.echo(
        f"PASS / NON-PASS: {len(res[res['filters'] == 'PASS'])} / {len(res[res['filters'] != 'PASS'])}"
    )
    click.echo(f"Mean AR for het: {het["AR"].mean():.4f} (+/- {het["AR"].std():.4f})")
    click.echo(f"Mean AR for hom: {hom["AR"].mean():.4f} (+/- {hom["AR"].std():.4f})")
    click.echo(f"Mean DP: {res["DP"].mean():.2f} (+/- {res["DP"].std():.2f})")
    N = ds.sample_count()

    click.echo(f"Het #: {len(het)}")
    click.echo(f"Hom #: {len(hom)}")
    click.echo(f"AF: {res['AC'].sum() / (2*N):.4f} (AC: {res['AC'].sum()}, AN: {2*N})")


if __name__ == "__main__":
    cli()
