import tiledbvcf

from common import dataset_write

@dataset_write
def ingest_samples(ds: tiledbvcf.Dataset, sample_uris: list[str]):
    ds.ingest_samples(sample_uris=[str(uri) for uri in sample_uris])

def ingest_sample(sample_uri: str):
    return ingest_samples([sample_uri])

