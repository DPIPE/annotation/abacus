import tempfile
import pytest
import tiledbvcf


@pytest.fixture(scope="function", autouse=True)
def tiledb_uri(monkeypatch):
    with tempfile.TemporaryDirectory() as temp_dir:
        monkeypatch.setenv("TILEDB_URI", temp_dir)
        tiledbvcf.Dataset(uri=temp_dir, mode="w").create_dataset()
        yield
