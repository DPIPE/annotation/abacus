from dataclasses import dataclass
import random
import string
import subprocess
from typing import Literal

import bgzip

from calculate_frequency import Sex


@dataclass
class HeaderField:
    field_type: Literal["FORMAT", "INFO"]
    key: str
    number: Literal["1", "A", "R"]
    type: Literal["Integer", "Float", "String"]
    description: str = "For testing purposes"

    def __str__(self):
        return f"##{self.field_type}=<ID={self.key},Number={self.number},Type={self.type},Description={self.description}>"


ALLOWED_FORMATS = {
    "GT": HeaderField("FORMAT", "GT", "1", "String", "Genotype"),
    "GP": HeaderField("FORMAT", "DP", "1", "Integer", "Read depth"),
    "AD": HeaderField("FORMAT", "AD", "R", "Integer", "Allelic depths"),
    "GQ": HeaderField("FORMAT", "GQ", "1", "Integer", "Genotype quality"),
}

ALLOWED_INFO = {
    "AF": HeaderField("INFO", "AF", "A", "Float", "Allele frequency"),
    "AC": HeaderField("INFO", "AC", "A", "Integer", "Allele count"),
    "AN": HeaderField("INFO", "AN", "1", "Integer", "Allele number"),
    "DP": HeaderField("INFO", "DP", "1", "Integer", "Read depth"),
}


@dataclass
class Variant:
    chromosome: str
    position: int
    ref: str = ""
    alt: str = ""
    quality: int = 5000
    filter: str = "PASS"
    info: dict = None
    format: dict[str, str] = None

    def __post_init__(self):
        if not self.format:
            self.format = {}
        else:
            disallowed_format_keys = set(self.format.keys()) - set(
                ALLOWED_FORMATS.keys()
            )
            assert (
                not disallowed_format_keys
            ), f"Disallowed format keys: {disallowed_format_keys}"

        if not self.info:
            self.info = {}
        else:
            disallowed_info_keys = set(self.info.keys()) - set(ALLOWED_INFO.keys())
            assert (
                not disallowed_info_keys
            ), f"Disallowed info keys: {disallowed_info_keys}"

        assert self.ref != self.alt
        assert len(self.ref) >= 1
        assert len(self.alt) >= 1

    def info_string(self):
        return (
            ";".join([f"{k}={v}" for k, v in self.info.items()]) if self.info else "."
        )

    def format_keys(self):
        return ":".join(self.format.keys()) if self.format else "."

    def format_values(self):
        return ":".join(self.format.values()) if self.format else "."

    def __str__(self):
        return "\t".join(
            [
                self.chromosome,
                str(self.position),
                ".",  # ID
                self.ref,
                self.alt,
                str(self.quality),
                self.filter,
                self.info_string(),
                self.format_keys(),
                self.format_values(),
            ]
        )


def generate_vcf(variants: list[Variant], sex: Literal[Sex.FEMALE, Sex.MALE]):
    """
    Generate a VCF file from a list of variants
    """
    sample_prefix = sex.name
    sample = (
        sample_prefix
        + "-"
        + "".join(random.choices(string.ascii_uppercase + string.digits, k=8))
    )

    header_lines = [
        "##fileformat=VCFv4.2",
        *[str(v) for v in ALLOWED_INFO.values()],
        *[str(v) for v in ALLOWED_FORMATS.values()],
        f"#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\t{sample}",
    ]
    yield from header_lines
    for v in variants:
        yield str(v)


def write_vcf(
    variants: list[Variant],
    filename: str,
    sex: Literal[Sex.FEMALE, Sex.MALE] | None = None,
):
    "Write a VCF file from a list of variants - bgzipped and indexed"
    with open(filename, "wb") as raw:
        with bgzip.BGZipWriter(raw) as fh:
            for line in generate_vcf(variants, sex or Sex.FEMALE):
                fh.write((line + "\n").encode())
    subprocess.run(["tabix", "-f", "-p", "vcf", filename])
