from calculate_frequency import variant_frequency
from ingest import ingest_samples
from tests.generation import Variant, write_vcf

def test_simple_case():
    variants = [
        Variant("1", 123, "A", "T", 100, "PASS", {}, {"GT": "0/1"})
    ]
    write_vcf(variants, "test.vcf.gz")
    ingest_samples(["test.vcf.gz"])
    for res in variant_frequency():
        print(res)