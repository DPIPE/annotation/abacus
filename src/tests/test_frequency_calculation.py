import os
from pathlib import Path
import pytest
import pandas as pd
import tiledbvcf
from calculate_frequency import variant_frequency, Sex
from genomes import Region
from ingest import ingest_samples
from tests.generation import Variant, write_vcf


@pytest.fixture(autouse=True)
def monkeypatch_compute_sex(monkeypatch):
    def mock(ds):
        samples = ds.samples()
        values = [getattr(Sex, s.split("-")[0]) for s in samples]
        series = pd.Series(values, index=samples)
        return series

    monkeypatch.setattr("calculate_frequency.sex_by_sample", mock)


@pytest.fixture(autouse=True)
def mock_regions(monkeypatch):
    monkeypatch.setattr("calculate_frequency.CHROMOSOMES", [Region("1", 1, 10000)])


def test_simple_case(tmp_path: Path):
    tmp_path = Path(".")
    variants = [Variant("1", 123, "A", "T", 100, "PASS", {}, {"GT": "0/1"})]
    write_vcf(variants, tmp_path / "test.vcf.gz")
    ingest_samples([tmp_path / "test.vcf.gz"])
    res = list(variant_frequency(10000))

    assert len(res) == 1, "Expected one result batch"
    assert len(res[0]) == 1, "Expected one variant"
    row = res[0].loc[0]
    assert row["contig"] == "1"
    assert row["pos_start"] == 123
    assert row["ref"] == "A"
    assert row["alt"] == "T"
    assert row["AC"] == 1
    assert row["AN"] == 2
    assert row["AF"] == 0.5


def test_multiple_samples(tmp_path: Path):
    for i in range(10):
        variants = [Variant("1", 123, "A", "T", 100, "PASS", {}, {"GT": "0/1"})]
        write_vcf(variants, tmp_path / f"test{i}.vcf.gz")
        ingest_samples([tmp_path / f"test{i}.vcf.gz"])
    res = list(variant_frequency(10000))
    assert len(res) == 1, "Expected one result batch"
    assert len(res[0]) == 1, "Expected one variant"
    row = res[0].loc[0]
    assert row["contig"] == "1"
    assert row["pos_start"] == 123
    assert row["ref"] == "A"
    assert row["alt"] == "T"
    assert row["AC"] == 10
    assert row["AN"] == 20
    assert row["AF"] == 0.5


def test_het_and_hom_zygosity(tmp_path: Path):
    for i in range(3):
        variants = [
            Variant("1", 123, "A", "T", 100, "PASS", {}, {"GT": "0/1"}),
            Variant("1", 124, "A", "T", 100, "PASS", {}, {"GT": "1/1"}),
        ]
        write_vcf(variants, tmp_path / f"test{i}.vcf.gz")
        ingest_samples([tmp_path / f"test{i}.vcf.gz"])

    for i in range(7):
        variants = [
            Variant("1", 123, "A", "T", 100, "PASS", {}, {"GT": "1/1"}),
            Variant("1", 124, "A", "T", 100, "PASS", {}, {"GT": "0/1"}),
        ]
        write_vcf(variants, tmp_path / f"test{i}.vcf.gz")
        ingest_samples([tmp_path / f"test{i}.vcf.gz"])

    res = list(variant_frequency(10000))
    assert len(res) == 1, "Expected one result batch"
    assert len(res[0]) == 2, "Expected two variants"
    for r in res[0].itertuples():
        if r.pos_start == 123:
            assert r.het == 3
            assert r.hom == 7
            assert r.AC == 17  # 7*2 + 3
            assert r.AN == 20
            assert r.AF == 0.85
        elif r.pos_start == 124:
            assert r.het == 7
            assert r.hom == 3
            assert r.AC == 13  # 3*2 + 7
            assert r.AN == 20
            assert r.AF == 0.65
        else:
            assert False, f"Unexpected variant at {r.pos_start}"


def test_hemi(monkeypatch, tmp_path: Path):
    monkeypatch.setattr("calculate_frequency.CHROMOSOMES", [Region("X", 1, 10000)])
    monkeypatch.setattr("calculate_frequency.PAR_REGIONS", [Region("X", 5000, 10000)])

    # Create 7 males, with 4 hemizygous in non-PAR regions,
    # and 1 heterozygous, and 1 homozygous variant in PAR regions
    for i in range(7):
        variants = [
            Variant("X", 1000, "A", "T", 100, "PASS", {}, {"GT": "1/1"}),
            Variant("X", 2000, "A", "T", 100, "PASS", {}, {"GT": "1"}),
            Variant(
                "X", 3000, "A", "T", 100, "PASS", {}, {"GT": "0/1"}
            ),  # Het calls are on sex chromosomes are treated as hemizygous
            Variant(
                "X", 6000, "A", "T", 100, "PASS", {}, {"GT": "1/1"}
            ),  # Except in PAR regions
            Variant("X", 7000, "A", "T", 100, "PASS", {}, {"GT": "0/1"}),
        ]
        write_vcf(variants, tmp_path / f"test{i}.vcf.gz", Sex.MALE)
        ingest_samples([tmp_path / f"test{i}.vcf.gz"])

    # Create 3 females with 2 heterozygous and 1 homozygous variant in non-PAR regions,
    # and 1 heterozygous, and 1 homozygous variant in PAR regions
    for i in range(3):
        variants = [
            Variant("X", 1000, "A", "T", 100, "PASS", {}, {"GT": "0/1"}),
            Variant("X", 2000, "A", "T", 100, "PASS", {}, {"GT": "0/1"}),
            Variant("X", 3000, "A", "T", 100, "PASS", {}, {"GT": "1/1"}),
            Variant("X", 6000, "A", "T", 100, "PASS", {}, {"GT": "0/1"}),
            Variant("X", 7000, "A", "T", 100, "PASS", {}, {"GT": "1/1"}),
        ]
        write_vcf(variants, tmp_path / f"test{i}.vcf.gz", Sex.FEMALE)
        ingest_samples([tmp_path / f"test{i}.vcf.gz"])

    res = list(variant_frequency(10000))
    assert len(res) == 1
    assert len(res[0]) == 5, "Expected 5 variants"
    for row in res[0].itertuples():
        if row.pos_start in [1000, 2000]:
            # GT 1/1 and GT 1 are both treated as hemizygous
            assert row.het == 3
            assert row.hom == 0
            assert row.hemi == 7
            assert row.AC == 10
            assert row.AN == 13  # 3*2 + 7
            assert f"{row.AF:5f}" == f"{10/13:5f}"  # == 0.76923
        elif row.pos_start == 3000:
            # GT 0/1 is treated as hemizygous
            assert row.het == 0
            assert row.hom == 3
            assert row.hemi == 7
            assert row.AC == 13  # 3*2 + 7
            assert row.AN == 13
            assert row.AF == 1.0
        elif row.pos_start in [6000]:
            # In PAR regions, GT 1/1 is treated as homozygous
            assert row.het == 3
            assert row.hom == 7
            assert row.hemi == 0
            assert row.AC == 17  # 7*2 + 3
            assert row.AN == 20
            assert row.AF == 0.85
        elif row.pos_start in [7000]:
            # In PAR regions, GT 0/1 is treated as heterozygous
            assert row.het == 7
            assert row.hom == 3
            assert row.hemi == 0
            assert row.AC == 13  # 3*2 + 7
            assert row.AN == 20
            assert row.AF == 0.65
        else:
            assert False, f"Unexpected variant at {row.pos_start}"
