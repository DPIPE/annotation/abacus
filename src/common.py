import glob
import os
from pathlib import Path
from pickle import dump, load

import tiledbvcf
from functools import wraps


def _dataset(func, mode):
    @wraps(func)
    def wrapper(*args, **kwargs):
        TILEDB_URI = os.environ["TILEDB_URI"]
        # Simple check to see if the dataset exists - not foolproof
        files = glob.glob(TILEDB_URI + "/*")
        if not files:
            raise ValueError(f"No dataset found at TILEDB_URI (= {TILEDB_URI}). Did you forget to create the dataset?")
        ds = tiledbvcf.Dataset(uri=TILEDB_URI, mode=mode)
        return func(ds, *args, **kwargs)
    return wrapper

def dataset_write(func):
    return _dataset(func, "w")

def dataset_read(func):
    return _dataset(func, "r")

def pickle_cache(cache_path):
    if cache_path:
        cache_path = Path(cache_path)
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            nonlocal cache_path
            if cache_path and os.path.exists(cache_path):
                with cache_path.open("rb") as cache:
                    cache = load(cache)
            else:
                cache = None
            res = func(*args, cache=cache, **kwargs)
            if cache_path:
                with cache_path.open("wb") as cache:
                    dump(res, cache)
            return res
        return wrapper
    return decorator