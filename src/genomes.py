import os

from dataclasses import dataclass


@dataclass
class Region:
    contig: str
    start: int
    end: int

    def __str__(self) -> str:
        return f"{self.contig}:{self.start}-{self.end}"

    def split(self, region_size: int):
        for i in range(self.start, self.end, region_size):
            yield Region(self.contig, i, min(i + region_size - 1, self.end))


def regions_overlap(r1: list[Region], r2: list[Region]) -> bool:
    for region1 in r1:
        for region2 in r2:
            if region1.contig == region2.contig:
                if region1.start < region2.end and region1.end > region2.start:
                    return True
    return False


GENOME_BUILD = os.environ["GENOME_BUILD"]
if GENOME_BUILD not in ["GRCh37", "GRCh38"]:
    raise ValueError(f"Invalid genome build '{GENOME_BUILD}'")

_CHROMOSOMES = {
    "GRCh37": [
        Region("1", 1, 249250621),
        Region("2", 1, 243199373),
        Region("3", 1, 198022430),
        Region("4", 1, 191154276),
        Region("5", 1, 180915260),
        Region("6", 1, 171115067),
        Region("7", 1, 159138663),
        Region("8", 1, 146364022),
        Region("9", 1, 141213431),
        Region("10", 1, 135534747),
        Region("11", 1, 135006516),
        Region("12", 1, 133851895),
        Region("13", 1, 115169878),
        Region("14", 1, 107349540),
        Region("15", 1, 102531392),
        Region("16", 1, 90354753),
        Region("17", 1, 81195210),
        Region("18", 1, 78077248),
        Region("19", 1, 59128983),
        Region("20", 1, 63025520),
        Region("21", 1, 48129895),
        Region("22", 1, 51304566),
        Region("X", 1, 155270560),
        Region("Y", 1, 59373566),
        Region("MT", 1, 16569),
    ],
    "GRCh38": [
        Region("1", 1, 248956422),
        Region("2", 1, 242193529),
        Region("3", 1, 198295559),
        Region("4", 1, 190214555),
        Region("5", 1, 181538259),
        Region("6", 1, 170805979),
        Region("7", 1, 159345973),
        Region("8", 1, 145138636),
        Region("9", 1, 138394717),
        Region("10", 1, 133797422),
        Region("11", 1, 135086622),
        Region("12", 1, 133275309),
        Region("13", 1, 114364328),
        Region("14", 1, 107043718),
        Region("15", 1, 101991189),
        Region("16", 1, 90338345),
        Region("17", 1, 83257441),
        Region("18", 1, 80373285),
        Region("19", 1, 58617616),
        Region("20", 1, 64444167),
        Region("21", 1, 46709983),
        Region("22", 1, 50818468),
        Region("X", 1, 156040895),
        Region("Y", 1, 57227415),
        Region("MT", 1, 16569),
    ],
}

_PAR_REGIONS = {
    "GRCh37": [
        Region("X", 60001, 2699520),
        Region("X", 154931044, 155260560),
    ],
    "GRCh38": [
        Region("X", 10001, 2781479),
        Region("X", 155701383, 156030895),
    ],
}

_NON_PAR_REGIONS = {
    "GRCh37": [
        Region("X", 2699521, 154931043),
    ],
    "GRCh38": [
        Region("X", 2781480, 155701382),
    ],
}

CHROMOSOMES = _CHROMOSOMES[GENOME_BUILD]
PAR_REGIONS = _PAR_REGIONS[GENOME_BUILD]
NON_PAR_REGIONS = _NON_PAR_REGIONS[GENOME_BUILD]
