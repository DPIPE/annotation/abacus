# Getting started

## Requirements

Everything needed to run abacus is a recent installation of docker/podman.

## Env file

The environment file `.env` shows required and optional environment variables needed to run abacus in a meaningful way. Set these to variables to fit your use case.

## docker-compose.yml

To take advantage of the .env file, we recommend using `docker compose`, running the compose file `docker-compose.yml`. This should not need any change.

# Running the Abacus CLI

To run the abacus CLI, you need to be inside an Abacus container, e.g. by executing `docker compose run -it --entrypoint bash abacus`. To run the CLI directly, you can execute `docker compose run abacus` from outside the container, or `abacus` from inside the container.

The CLI is based on [click](https://click.palletsprojects.com/en/stable/). The CLI is self-documented, so running `abacus --help` will give you a help text. Similarly, for all subcommands: `abacus <subcommand> --help`.

# Using the TileDB-VCF CLI

You can also use the TileDB-VCF CLI directly from the container. See [TileDB-VCF docs](https://tiledb-inc.github.io/TileDB-VCF/documentation/api-reference/cli.html) for more information.


