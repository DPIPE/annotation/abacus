$~$

<div align="center">
    <img width="350px" style="border: 0;" src="docs/logo/Abacus_logo_blue.svg" alt="Abacus"/>
</div>

$~$

Abacus is a genetic variant instance and frequency database that scales to thousands of human genomes.

## License and copyright

Abacus   
Copyright (C) 2023 Abacus contributors

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program. If not, see https://www.gnu.org/licenses/.
