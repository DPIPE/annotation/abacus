FROM python:3.12-slim AS base

RUN apt update && \
    apt install -y \
    tabix \
    zlib1g-dev \
    libbz2-dev \
    liblzma-dev && \ 
    apt clean && \
    rm -rf /var/lib/apt/lists/*

ENV VIRTUAL_ENV=/opt/.venv
ENV PATH=/opt/.venv/bin:${PATH}
WORKDIR /opt

RUN pip install --no-cache-dir uv

FROM base AS deps
# Create a virtual environment
WORKDIR /opt
RUN apt update && \
    apt install -y \
    gcc && \
    apt clean && \
    rm -rf /var/lib/apt/lists/*
COPY pyproject.toml uv.lock /opt/
RUN uv sync --no-cache

FROM deps AS tiledb
RUN apt update && \
    apt install -y \
    autoconf \
    automake \
    cmake \
    git \
    gcc \
    g++ && \
    apt clean && \
    rm -rf /var/lib/apt/lists/*

# # Install TileDB-VCF - this is a bit of a hack, but it works
# # Need to turn off WERROR because of a warning in the TileDB-VCF code
# # TODO: Build libtiledbvcf as a separate layer, and include python API in uv lock file
WORKDIR /
RUN git clone https://github.com/TileDB-Inc/TileDB-VCF && \
    cd TileDB-VCF && \
    git checkout 0.36.0 && \
    cd libtiledbvcf && \
    mkdir build && \
    cd build && \
    cmake -D TILEDB_WERROR=OFF .. && \
    cd /TileDB-VCF/apis/python && \
    uv pip install . --no-cache && \
    mkdir /opt/lib && cp /TileDB-VCF/dist/lib/* /opt/lib/ && \
    rm -rf /TileDB-VCF

FROM base AS final
COPY --from=deps /opt/ /opt/
COPY --from=tiledb /opt/ /opt/
COPY --from=deps /usr/lib/x86_64-linux-gnu/libgomp.so.1 /opt/lib/

ENV LD_LIBRARY_PATH=/opt/lib

ENV PYTHONPATH=/app/src

COPY . /app
WORKDIR /app
RUN uv pip install --no-cache .
ENTRYPOINT [ "abacus" ]
CMD ["--help"]
